$(document).ready(function() {
    $('.hamburger').on("click", function (){
        $(this).toggleClass('is-active');
    });
    var idCat = $("form").attr('id');
    $("input[type='radio']").on("change", function (){
        var savethis = $(this);
        var form = savethis.closest('form');
        $.ajax({
            url: form.data('ajax-url'),
            method: 'POST',
            data: form.serialize(),
            context: document.body
        }).done(function(response) {
            $('#give-me-all').empty().html($(response).filter('#'+idCat).html());
            //$('#give-me-all').empty().html(response);
        });
    });
    var $map, $sidebar;
    function map($map, $sidebar){
        var mapHeight = $($map).outerHeight();
        var inputHeight = $('.form-part').outerHeight();
        if(mapHeight && inputHeight){
            $($sidebar).css('height', mapHeight - inputHeight)
        }
    }
    map('#map', '#sl_sidebar');
    $(window).resize(function(){
        map('#map', '#sl_sidebar');
    })
});
