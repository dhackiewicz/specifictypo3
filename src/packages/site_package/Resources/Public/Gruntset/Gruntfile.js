module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {                              // Task
            dist: {
                options:{
                    style:'compressed'
                },
                files: {                         // Dictionary of files
                    '../Css/dest/app.css': '../Css/src/*.scss'
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            my_target: {
                files: {
                    '../JavaScript/dest/app.min.js': '../JavaScript/src/app.js',
                }
            }
        },
        concat: {
            options: {
                separator: '',
            },
            js: {
                src: ['../JavaScript/src/app.js', '../JavaScript/libs/jquery-3.2.1.js', '../JavaScript/libs/bootstrap.bundle.min.js'],
                dest: '../JavaScript/dest/app.min.js',
            },
            css: {
                src: ['../Css/libs/bootstrap.min.css',
                    '../Css/libs/aos.css',
                    '../Css/dest/app.min.css'
                ],
                dest: '../Css/dest/app.min.css',
            },
        },
        jshint: {
            main: '../JavaScript/dest/app.min.js',
            reporterOutput: '',
        },
        watch: {
            css: {
                files: ['../Css/src/*.scss','../JavaScript/src/*.js'],
                tasks: ['default'],
            }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    hostname: 'specific.localhost',
                }
            }
        },
        postcss: {
            options: {
                map: true, // inline sourcemaps
                processors: [
                    require('pixrem')(), // add fallbacks for rem units
                    require('autoprefixer')({overrideBrowserslist: 'last 4 versions'}), // add vendor prefixes
                    require('cssnano')() // minify the result
                ]
            },
            dist: {
                src: 'css/*.css'
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-postcss');

    // Default task(s).
    grunt.registerTask('default', ['sass', 'uglify']);
};
