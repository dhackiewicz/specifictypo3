<?php
#Rejestracja static templates (config+constans)
defined('TYPO3_MODE') or die();

call_user_func(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        'map',
        'Configuration/Typoscript',
        'Map Extension TYPO3');
});