<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {

    #Registration in Backend
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Studio.Map',
        'Pi1',
        'Map TYPO3 Extension'
    );

    #
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['map_pi1'] = 'pi_flexform';

    #
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        'map_pi1',
        'FILE:EXT:map/Configuration/FlexForms/flexform_pi1.xml'
    );
});