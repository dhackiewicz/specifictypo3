<?php
defined('TYPO3_MODE') or die();

return call_user_func(function () {
    return [
        'ctrl' => [
            'title' => 'Location',
            'label' => 'name',
            'label_alt' => 'address',
            'label_alt_force' => true,
            'tstamp' => 'tstamp',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'origUid' => 't3_origuid',
            'languageField' => 'sys_language_uid',
            //'transOrigPointerField' => 'l10n_parent',
            'transOrigPointerField' => 'l18n_parent',
            //'transOrigDiffSourceField' => 'l10n_diffsource',
            'transOrigDiffSourceField' => 'l18n_diffsource',
            'translationSource' => 'l10n_source',
            'delete' => 'deleted',
            'sortby' => 'sorting',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'hideTable' => false,
            'searchFields' => 'street',
            'iconfile' => 'EXT:map/Resources/Public/Icons/ext.svg',
        ],
        'interface' => [
            'showRecordFieldList' => 'name, address, url, description, phone, email, latitude, longitude  '
        ],
        'types' => [
            '0' => ['showitem' => 'name, address, url, description, phone, email, latitude, longitude']
        ],
        'palettes' => [
            '0' => ['showitem' => '']
        ],
        'columns' => [
            'name' => [
                'exclude' => 1,
                'label' => 'Name',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'address' => [
                'exclude' => 1,
                'label' => 'Address',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'url' => [
                'exclude' => 1,
                'label' => 'Url',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'description' => [
                'exclude' => 1,
                'label' => 'Description',
                'config' => [
                    'type' => 'text',
                    'eval' => 'trim',
                ],
            ],
            'phone' => [
                'exclude' => 1,
                'label' => 'Phone',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'email' => [
                'exclude' => 1,
                'label' => 'Email',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'latitude' => [
                'exclude' => 1,
                'label' => 'Latitude',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'longitude' => [
                'exclude' => 1,
                'label' => 'Longitude',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'destination' => [
                'exclude' => 1,
                'label' => 'Destination',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ]
        ]
    ];
});