<?php

/**
 * Extension Manager/Repository config file for ext "google maps made by Studio Thom Pfister"
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'map',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.00-10.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Studio\\Map\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Daniel Hackiewicz',
    'author_email' => 'd.hackiewicz@commix.ch',
    'author_company' => 'Studio Thom Pfister',
    'version' => '1.0.0',
];
