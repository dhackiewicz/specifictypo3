CREATE TABLE tx_map_domain_model_location (
	name varchar(255) NOT NULL DEFAULT '',
	address varchar(255) NOT NULL DEFAULT '',
	url varchar(255) NOT NULL DEFAULT '',
	description varchar(255) NOT NULL DEFAULT '',
	phone varchar(255) NOT NULL DEFAULT '',
	email varchar(255) NOT NULL DEFAULT '',
	latitude float(5,10) NOT NULL DEFAULT 0,
	longitude float(5,10) NOT NULL DEFAULT 0,
	destination int(11) NOT NULL DEFAULT 0,
	sorting int(11) NOT NULL DEFAULT 0
);