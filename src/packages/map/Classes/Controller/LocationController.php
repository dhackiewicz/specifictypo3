<?php
declare(strict_types=1);

namespace Studio\Map\Controller;

use Studio\Map\Domain\Model\Location;
use \Studio\Map\Domain\Repository\LocationRepository;

class LocationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \Studio\Map\Domain\Repository\LocationRepository
     */
    protected $locationRepository;

    /**
     * @param \Studio\Map\Domain\Repository\LocationRepository $locationRepository
     * @return void
     */
    public function injectLocationRepository(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * @return void
     */
    public function listAction()
    {

    }

    /**
     * @return void
     */
    public function listAjaxAction(string $code = '')
    {
        $jsonData = [];
        $locations = $this->locationRepository->findAll();

        //$wszystkie

        /**
         * @var string $key
         * @var Location $location
         */
        foreach ($locations as $key => $location) {
            $jsonData[] = [
                'id' => $location->getUid(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'url' => $location->getUrl(),
                'description' => $location->getDescription(),
                'phone' => $location->getPhone(),
                'lat' => $location->getLatitude(),
                'lng' => $location->getLongitude(),
                //'distance' => 150,
            ];
        }
        //sortowanie po kluczu distance

        $this->view->assignMultiple([
            'locations' => $jsonData,
        ]);
    }

}

