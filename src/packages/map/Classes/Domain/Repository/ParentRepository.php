<?php
declare(strict_types=1);

namespace Studio\Map\Domain\Repository;

class ParentRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    ###Definition of search area
    /**
     * @return void
     */
    public function initializeObject()
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings */
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(false);

        $this->setDefaultQuerySettings($querySettings);
    }



}

