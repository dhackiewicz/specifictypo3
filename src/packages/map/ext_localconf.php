<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Studio.Map',
        'Pi1',
        [
            'Location' => 'list',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Studio.Map',
        'Pi2',
        [
            'Location' => 'listAjax',
        ]
    );
});


