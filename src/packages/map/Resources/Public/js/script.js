"use strict";
var actualLat = '';
var actualLng = '';
var addressLatLng;
var map;
var newvet;
var list = []; // For display as list
var markers = [];
var markersIds = [];
var withDirection = [];
var allPlaces = [];
var allPlacesSelected = [];
var destinations = [];
var infowindow;
var prev_infowindow = false;
var address = [];


$(document).ready(function() {
	start();
});

function strpos(haystack, needle, offset) {
	var i = (haystack + '').indexOf(needle, (offset || 0));
	return i === -1 ? false : i;
}

//Starts here on Page Load
function start() {

	var lat='';
	var lng='';

	//Check if there is any valu in input -> TASK, Priorität 2
	if ($("#map-search").length>0 && $("#map-search").val()!='') {
		var address = $('#map-search').val();
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( {'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var latLng = String(results[0].geometry.location);
				latLng = latLng.substr(1);
				var pos = strpos(latLng, ',');
				lat = latLng.substr(0,pos);
				var pos2 = strpos(latLng, ')');
				latLng = latLng.substr(0,pos2);
				lng = latLng.substr((pos+2));
				var addressLatLng = (results[0].geometry.location);
				//LAT & LNG of the searched Address
				actualLat = lat;
				actualLng = lng;
				initLocalisation();
			}
		});

	} else {
		initLocalisation();
	}
}

jQuery('#reset-this-mate').on('click', function (){
	clearLocations();
	initLocalisation();
})

//Execute Second on Locator Load
function initLocalisation() {
	init_map();
	createAllMarkers();
	//createDestinations();
	display_locations_list();
}

function init_map() {
	var mapOptions = {
		center: new google.maps.LatLng(46.80120961248365, 8.226644086867777),
		zoom: 8,
		mapTypeControl: false,
		styles: style
	};
	map = new google.maps.Map(document.getElementById("map"),
		mapOptions);
}


function display_locations_list() {
	$.getJSON("/?type=123", function(json1) {
		$.each(json1, function(key, data) {
			newvet = `
			<div class="store_locator_sidebar_entry sidebar_entry_btn" data-id="${data.id}" data-lat="${data.lat}" data-lng="${data.lng}"> 
				<b> ${data.name} </b><br/>
				${data.address}
			</div>
			`;
			list.push(newvet);
			return list;
		});
		$('.number-of-vets').html(list.length);
		$('#sl_sidebar').html(list);
	});
}

jQuery(document).on('click', ".sidebar_entry_btn", function(event) {
	event.preventDefault();
	var id = $(this).attr('data-id');
	var lat = $(this).attr('data-lat');
	var lng = $(this).attr('data-lng');
	var latlng = new google.maps.LatLng(
		parseFloat(lat),
		parseFloat(lng)
	);
	var bounds = new google.maps.LatLngBounds();
	bounds.extend(latlng);
	map.setCenter(bounds.getCenter());
	map.setZoom(10);
	google.maps.event.trigger(markers[id], 'click');
});

//Sort dynamic
function dynamicSort(property) {
	var sortOrder = 1;
	if(property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a,b) {
		/* next line works with strings and numbers,
         * and you may want to customize it to your needs
         */
		var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		return result * sortOrder;
	}
}

jQuery(document).on('click', "#map-find", function(event) {
	var lat='';
	var lng='';
	var allPlaces = [];




	if ($("#map-search").length>0 && $("#map-search").val()!='') {
		var address = $('#map-search').val();

		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( {'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				//Change the center of the map
				//map.setCenter(results[0].geometry.location);
				//Marker with searched Position
				//new google.maps.Marker({
				//	map,
				//		position: results[0].geometry.location,
				//});
				//Change the zoom of the map
				//if (results[0].geometry.viewport)
				//	map.fitBounds(results[0].geometry.viewport);

				var latLng = String(results[0].geometry.location);
				latLng = latLng.substr(1);
				var pos = strpos(latLng, ',');
				lat = latLng.substr(0,pos);
				var pos2 = strpos(latLng, ')');
				latLng = latLng.substr(0,pos2);
				lng = latLng.substr((pos+2));

				var latlng = new google.maps.LatLng(
					parseFloat(lat),
					parseFloat(lng)
				);

				var bounds = new google.maps.LatLngBounds();
				bounds.extend(latlng);
				map.setCenter(bounds.getCenter());
				map.setZoom(10);


				addressLatLng = (results[0].geometry.location);
				$.getJSON("/?type=123", function(json1) {
					$.each(json1, function(key, data) {

						withDirection.push(data);
						var lat1 = lat;
						var lon1 = lng;
						var lat2 = data.lat;
						var lon2 = data.lng;
						var selectedDistances = [];

						function calcCrow(lat1, lon1, lat2, lon2)
						{
							var R = 6371; // km
							var dLat = toRad(lat2-lat1);
							var dLon = toRad(lon2-lon1);
							var lat1 = toRad(lat1);
							var lat2 = toRad(lat2);

							var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
								Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
							var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
							var d = R * c;
							return d;
						}
						//console.log(calcCrow(lat1, lon1, lat2, lon2).toFixed(0));
						var selectedDistances = [];
						selectedDistances = calcCrow(lat1, lon1, lat2, lon2).toFixed(0);

						// Converts numeric degrees to radians
						function toRad(Value)
						{
							return Value * Math.PI / 180;
						}
						var destDirect = calcCrow(lat1, lon1, lat2, lon2).toFixed(0);
						destDirect = parseInt(destDirect);
						data.direct = destDirect;
					});

					withDirection.sort(dynamicSort("direct"));
					for (var i = 0; i < 5; i++) {
						allPlacesSelected.push(withDirection[i]);
					}
					withDirection = [];
					distancebetween(addressLatLng, allPlacesSelected);
					allPlacesSelected = [];
				});
			}
		});

	} else {
		clearLocations();
		start();
	}

});

/*Clear sidepanel */
function clearLocations() {
	$("#sl_sidebar").empty();
	for (var i = 0; i < markersIds.length; i++) {
		if(markersIds[i]>0) markers[markersIds[i]].setMap(null);
	}
	markersIds = [];
	list = [];
}

//Distance between two points
function distancebetween(zip, allPlacesSelected) {
	var selected = allPlacesSelected;
	//Remove brackets
	zip = zip.toString().replace("(","").replace(")","");
	//Address to array
	address.push(zip);

	//Adding lat & nlg to destinations from AllPlaces

	for (let i = 0; i < 5; i++) {
		destinations.push(selected[i].lat + ',' + selected[i].lng)
	}

	const service = new google.maps.DistanceMatrixService(); // instantiate Distance Matrix service
	const matrixOptions = {
		origins: address,
		destinations: destinations, // technician locations
		travelMode: 'DRIVING',
		unitSystem: google.maps.UnitSystem.METRIC
	};
	// Call Distance Matrix service
	service.getDistanceMatrix(matrixOptions, callback);
	// Callback function used to process Distance Matrix response
	function callback(response, status) {

		if (status !== "OK") {
			alert("Error with matrix");
			clearLocations();
			start();
		} else {
			var origins = response.originAddresses;
			var destinations = response.destinationAddresses;
			for (var i = 0; i < origins.length; i++) {
				var results = response.rows[i].elements;
				for (var j = 0; j < results.length; j++) {
					var element = results[j];
					var distance = element.distance.text;
					var dest = distance;
					dest = dest.replace(' km', '');
					dest = parseInt(dest);
					selected[j].destination = dest;
				}
			}
			selected.sort(dynamicSort("destination"));
			clearLocations();
			display_locations_list_from_allPlaces(selected);
		}
	}
	//Clear addresses & destinations
	address = [];
	destinations = [];
}


function display_locations_list_from_allPlaces(obj){

	//Comparing based on the property qty
	function compare_qty(a, b){
		// a should come before b in the sorted order
		if(a.destination < b.destination){
			return -1;
			// a should come after b in the sorted order
		}else if(a.destination > b.destination){
			return 1;
			// a and b are the same
		}else{
			return 0;
		}
	}
	//Sort
	obj.sort(compare_qty);
	//Slice numbers of locations
	obj.forEach(function(value){
		//Creating Markers with selected
		createOneMarker(value);
	})


	if(obj != ''){
		obj.forEach(function (item) {
			newvet = `
			<div class="store_locator_sidebar_entry sidebar_entry_btn" data-id="${item.id}" data-lat="${item.lat}" data-lng="${item.lng}"> 
				<b> ${item.name} </b><br/>
				${item.address} <br/><span class="txt-red">(${item.destination} km)</span>  
			</div>
			`;
			list.push(newvet);
			return list;
		})
		$('.number-of-vets').html(list.length);
		$('#sl_sidebar').html(list);
	}
	allPlaces = [];
}

//Show all markers on map
function createAllMarkers() {

	$.getJSON("/?type=123", function(json1) {
		$.each(json1, function(key, data) {
			allPlaces.push(data);
			var id = data.id;
			var icon = {
				url: '/typo3conf/ext/map/Resources/Public/img/filiale-s.svg',
				scaledSize: new google.maps.Size(50, 50), // scaled size
			};
			var latLng = new google.maps.LatLng(data.lat, data.lng);
			// Creating a marker and putting it on the map
			var marker = new google.maps.Marker({
				position: latLng,
				map: map,
				title: data.name,
				icon:icon,
				id: data.id,
				clickable: true,
				animation: google.maps.Animation.DROP
			});

			marker.setMap(map);

			markers[id] = marker;
			markersIds.push(id);

			const contentString =
				'<div id="content">' +
				'<div id="siteNotice">' +
				"</div>" +
				'<h4 id="firstHeading" class="firstHeading">'+ data.name+'</h4>' +
				'<div id="bodyContent">' +
				"<p>"+data.address + '<br/>'+
				data.description + '<br/>'+
				'<a href="tel:'+data.phone+'">'+data.phone+'</a><br/>'+
				data.url+"</p>"
			"</div>" +
			"</div>";

			marker.addListener("click", () => {
				infowindow = new google.maps.InfoWindow({
					content: contentString,
				});
				infowindow.open(map, marker);

				if( prev_infowindow ) {
					prev_infowindow.close();
				}

				prev_infowindow = infowindow;
				//infowindow.open(map, marker);
			});

		});
	});
}



//Trying to create only one marker in loop
function createOneMarker(obj) {
	console.log(obj);
	var id = obj.id;
	var latlng = obj.lat + ', ' + obj.lng;
	var lat = obj.lat;
	var lng = obj.lng;
	var name = obj.name;
	var address = obj.address;
	var url = obj.url;
	var description = obj.description;
	var phone = obj.phone;

	if(id==undefined) id = '';
	if(latlng==undefined) latlng = '';
	if(lat==undefined) lat = '';
	if(lng==undefined) lng = '';
	if(name==undefined) name = '';
	if(address==undefined) address = '';
	if(phone==undefined) phone = '';
	if(description==undefined) description = '';
	if(url==undefined) url = '';

	var icon = {
		url: '/typo3conf/ext/map/Resources/Public/img/filiale-s.svg',
		scaledSize: new google.maps.Size(50, 50), // scaled size
	};

	// Icon
	var latLng = new google.maps.LatLng(lat, lng);

	// Creating a marker and putting it on the map
	var marker = new google.maps.Marker({
		position: latLng,
		map: map,
		icon:icon,
		id: id,
		animation: google.maps.Animation.DROP
	});

	marker.setMap(map);

	markers[id] = marker;
	markersIds.push(id);

	const contentStringTwo =
		'<div id="content">' +
		'<div id="siteNotice">' +
		"</div>" +
		'<h4 id="firstHeading" class="firstHeading">'+ name+'</h4>' +
		'<div id="bodyContent">' +
		"<p>"+ address + '<br/>'+
		description + '<br/>'+
		'<a href="tel:'+phone+'">'+phone+'</a><br/>'+
		url+"</p>"
	"</div>" +
	"</div>";


	marker.addListener("click", () => {
		infowindow = new google.maps.InfoWindow({
			content: contentStringTwo,
		});
		infowindow.open(map, marker);

		if( prev_infowindow ) {
			prev_infowindow.close();
		}

		prev_infowindow = infowindow;
		//infowindow.open(map, marker);
	});

}



var style = [
	{
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#f5f5f5"
			}
		]
	},
	{
		"elementType": "labels.icon",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#616161"
			}
		]
	},
	{
		"elementType": "labels.text.stroke",
		"stylers": [
			{
				"color": "#f5f5f5"
			}
		]
	},
	{
		"featureType": "administrative",
		"elementType": "labels",
		"stylers": [
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "administrative.country",
		"elementType": "geometry.stroke",
		"stylers": [
			{
				"color": "#363333"
			},
			{
				"visibility": "on"
			},
			{
				"weight": 1
			}
		]
	},
	{
		"featureType": "administrative.land_parcel",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#bdbdbd"
			}
		]
	},
	{
		"featureType": "administrative.province",
		"elementType": "geometry.fill",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#eeeeee"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#757575"
			}
		]
	},
	{
		"featureType": "poi.park",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#e5e5e5"
			}
		]
	},
	{
		"featureType": "poi.park",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#9e9e9e"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#ffffff"
			}
		]
	},
	{
		"featureType": "road.arterial",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#757575"
			}
		]
	},
	{
		"featureType": "road.highway",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#dadada"
			}
		]
	},
	{
		"featureType": "road.highway",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#616161"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#9e9e9e"
			}
		]
	},
	{
		"featureType": "transit.line",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#e5e5e5"
			}
		]
	},
	{
		"featureType": "transit.station",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#eeeeee"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#c9c9c9"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#9e9e9e"
			}
		]
	}
]