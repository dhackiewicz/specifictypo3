<?php
declare(strict_types = 1);

return [
    \Studio\Product\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
    ],
];