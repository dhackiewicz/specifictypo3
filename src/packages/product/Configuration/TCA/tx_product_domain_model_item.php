<?php
defined('TYPO3_MODE') or die();

return call_user_func(function () {
    return [
        'ctrl' => [
            'title' => 'Product',
            'label' => 'name',
            'tstamp' => 'tstamp',
            'thumbnail' => 'image',
            'crdate' => 'crdate',
            'cruser_id' => 'cruser_id',
            'origUid' => 't3_origuid',
            'languageField' => 'sys_language_uid',
            //'transOrigPointerField' => 'l10n_parent',
            'transOrigPointerField' => 'l18n_parent',
            //'transOrigDiffSourceField' => 'l10n_diffsource',
            'transOrigDiffSourceField' => 'l18n_diffsource',
            'translationSource' => 'l10n_source',
            'delete' => 'deleted',
            'sortby' => 'sorting',
            'enablecolumns' => [
                'disabled' => 'hidden',
                'starttime' => 'starttime',
                'endtime' => 'endtime',
            ],
            'hideTable' => false,
            'searchFields' => 'name',
            'iconfile' => 'EXT:product/Resources/Public/Icons/ext.svg',
        ],
        'interface' => [
            'showRecordFieldList' => 'name, additional, image, description, url'
        ],
        'types' => [
            '0' => ['showitem' => 'name, additional, image, description, url']
        ],
        'palettes' => [
            '0' => ['showitem' => '']
        ],
        'columns' => [
            'name' => [
                'exclude' => 1,
                'label' => 'Name',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'additional' => [
                'exclude' => 1,
                'label' => 'Additional name',
                'config' => [
                    'type' => 'input',
                    'eval' => 'trim',
                ],
            ],
            'image' => [
                'exclude' => 1,
                'label' => 'Image',
                'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                    'image',
                    [
                        'maxitems' => 3
                    ],
                    'jpg,jpeg,png'
                )
            ],
            'description' => [
                'exclude' => 1,
                'label' => 'Description',
                'config' => [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 15,
                    'enableRichtext' => true
                ],
            ],
            'url' => [
                'exclude' => 1,
                'label' => 'Link',
                'config' => [
                    'type' => 'input',
                    'renderType' => 'inputLink',
                ],
            ]
        ]
    ];
});