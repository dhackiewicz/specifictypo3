<?php
#Rejestracja static templates (config+constans)
defined('TYPO3_MODE') or die();

#Adding Typoscript
call_user_func(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        'product',
        'Configuration/Typoscript',
        'Product Extension TYPO3');
});