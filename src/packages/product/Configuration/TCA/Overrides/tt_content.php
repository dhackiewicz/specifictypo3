<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {

    #Registration in Backend
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Studio.Product',
        'Pi1',
        'Products with Filter made by Studio Thom Pfister'
    );

    #Registration in Backend
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Studio.Product',
        'Pi2',
        'Dogs'
    );

    #Adding flex form
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['product_pi1'] = 'pi_flexform';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        'product_pi1',
        'FILE:EXT:product/Configuration/FlexForm/flexform_pi1.xml'
    );
});
