<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
        'product',
        'tx_product_domain_model_item',
        'categories' // this parameter is optional! default value is categories
    );
});

