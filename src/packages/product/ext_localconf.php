<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Studio.Product',
        'Pi1',
        [
            'Item' => 'list',
        ]
    );
   \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Studio.Product',
        'Pi2',
        [
            'Item' => 'search',
        ]
    );
});
