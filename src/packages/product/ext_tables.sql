CREATE TABLE tx_product_domain_model_item (
    name varchar(255) NOT NULL DEFAULT '',
    additional varchar(255) NOT NULL DEFAULT '',
    image int(11) NOT NULL DEFAULT 0,
    description varchar(255) NOT NULL DEFAULT '',
    url varchar(255) NOT NULL DEFAULT '',
    categories int(11) NOT NULL DEFAULT 0,
    sorting int(11) NOT NULL DEFAULT 0
);