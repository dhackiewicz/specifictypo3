<?php

/**
 * Extension Manager/Repository config file for ext "List of Products with advanced filtering options made by Studio Thom Pfister"
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'product',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.00-10.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Studio\\Product\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Daniel Hackiewicz',
    'author_email' => 'dani@studiothompfister.com',
    'author_company' => 'Studio Thom Pfister',
    'version' => '1.0.0',
];
