<?php
declare(strict_types=1);

namespace Studio\Product\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class ItemRepository extends ParentRepository
{
    /**
     * @param array $categories
     * @return QueryResultInterface
     */
    public function findByCategories(array $categories): QueryResultInterface
    {
        $constraints = [];
        $query = $this->createQuery();

        foreach ($categories as $category) {
            $constraints[] = $query->contains('categories', $category);
        }

        $query->matching($query->logicalAnd($constraints));

        return $query->execute();
    }


//OLD
//    /**
//     * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface $categories
//     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
//     */
//    public function findByCategory($categories)
//    {
//
//        $query = $this->createQuery();
//
//        $constraints = [];
//
//        for ($categories->rewind(); $categories->valid(); $categories->next())
//        {
//            $constraints[] = $query->contains('categories', $categories->current());
//        }
//
//        $query->matching(
//            $query->logicalOr($constraints)
//        );
//
//        $results = $query->execute();
//
//        return $results;
//    }

}