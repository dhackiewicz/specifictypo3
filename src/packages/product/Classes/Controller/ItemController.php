<?php
declare(strict_types=1);

namespace Studio\Product\Controller;

use \Studio\Product\Domain\Repository\ItemRepository;
use \Studio\Product\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class ItemController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \Studio\Product\Domain\Repository\ItemRepository
     */
    protected $itemRepository;

    /**
     * @var \Studio\Product\Domain\Repository\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @param \Studio\Product\Domain\Repository\ItemRepository $itemRepository
     * @return void
     */
    public function injectItemRepository(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param \Studio\Product\Domain\Repository\CategoryRepository $categoryRepository
     * @return void
     */
    public function injectCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function listAction()
    {
        $categories = $this->categoryRepository->findAll();
        $items = $this->itemRepository->findAll();

        $this->view->assignMultiple([
            'items' => $items,
            'categories' => $categories,
        ]);
    }

    public function searchAction()
    {
        $searched = [];

        if ($this->request->hasArgument('flexRadioDefault')) {
            $filterCategories = $this->request->getArgument('flexRadioDefault');
        }

        if ($filterCategories !== null && is_array($filterCategories)) {
            $filterCategories = array_values(array_filter($filterCategories, function ($item) {
                return (int) $item !== -1;
            }));
        }

        if ($filterCategories !== []) {
            $searched = $this->itemRepository->findByCategories($filterCategories);
        } else {
            $searched = $this->itemRepository->findAll();
        }

        $this->view->assignMultiple([
            'searched' => $searched,
        ]);
    }

}
